from django.urls import path

from .views import StudentDetailView

app_name = 'students'
urlpatterns = [
    path('<int:id>/', StudentDetailView.as_view(), name='student-detail'),
]
