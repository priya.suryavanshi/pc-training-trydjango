from django.shortcuts import render
from django.views import View

from .models import Student


class StudentDetailView(View):
    template_name = 'students/student_detail.html'

    def get(self, request, id=None, *args, **kwargs):
        obj = Student.objects.get(id=id)
        context = {
            "student": obj
        }
        return render(request, self.template_name, context)

