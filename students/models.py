from django.db import models
from django.urls import reverse

import datetime
from mongoengine import [
                Document,
                StringField,
                IntField,
                ListField,
                ReferenceField,
                EmbeddedDocumentListField
                ]
from courses.models import Course

class Student(models.Model):
    name = models.CharField(max_length=120)
    age = models.IntegerField()
    courses_taken = models.ManyToManyField(Course, through='Enrollment')

    def __str__(self):
        return str(self.id)+'-'+self.name

    def get_absolute_url(self):
        return reverse('students:student-detail', kwargs={"id": self.id})

    


class Enrollment(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    credits_taken = models.PositiveIntegerField(default=4)
    final_grade = models.CharField(max_length=1, blank=True, null=True)
    

    class Meta:
        unique_together = [['student', 'course']]




# class Student(Document):
#     name = StringField(max_length=120, required=True)
#     age = IntField(required=True)
#     courses_taken = ListField(ReferenceField(Course))
#     enrollments = EmbeddedDocumentListField(Enrollment))

#     def __str__(self):
#         return str(self.id)+'-'+self.name

#     def get_absolute_url(self):
#         return reverse('students:student-detail', kwargs={"id": self.id})

    


# class Enrollment(EmbededDocument):
#     student = ObjectIdField(required=True)
#     course = reference(required=True)

#     credits_taken = IntField(default=4, required=True)
#     final_grade = models.CharField(max_length=1, blank=True, null=True)
    

#     meta = {
#     'indexes': [
#             {'fields': ('student', 'course'), 'unique': True}
#         ]
#     }

