import csv

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.views import View
from django.db.models import Q

from students.models import Student
from .forms import CourseModelForm
from .models import Course


class CourseObjectMixin(object):
    model = Course

    def get_object(self):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(self.model, id=id)
        return obj
    

class DownloadCsvCourseStudentsListView(View):

    def get(self, request, *args, **kwargs):
    
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="coursess.csv"' # your filename
        writer = csv.writer(response)
        writer.writerow(['id', 'course_id', 'course_name', 'student_name', 'credits_taken'])
        csv_id = 1
        queryset = Course.objects.all()
        course_queryset = queryset
        student_queryset = []
        query = request.GET.get("q")

        if query:
            queryset = queryset.filter(
                Q(title__icontains=query) |
                Q(student__name__icontains=query)
                ).distinct()
            
            course_queryset = queryset.filter(title__icontains=query)
            student_queryset = Student.objects.filter(name__icontains=query)

        for course in queryset:
            if not query or query=='' or course in course_queryset:
                for student in course.student_set.all():
                    enrollment = student.enrollment_set.get(course=course)
                    writer.writerow([
                        csv_id, 
                        course.id, 
                        course.title, 
                        student.name, 
                        enrollment.credits_taken
                    ])
                    csv_id += 1
            else:
                for student in course.student_set.all():
                    if student in student_queryset:
                        enrollment = student.enrollment_set.get(course=course)
                        writer.writerow([
                            csv_id, 
                            course.id,
                            course.title, 
                            student.name, 
                            enrollment.credits_taken
                        ])
            
        return response


class CourseStudentsListView(View):
    template_name = "courses/course_students_list.html"

    def get(self, request, *args, **kwargs):
        queryset = Course.objects.all()
        course_queryset = queryset
        student_queryset = []
        query = request.GET.get("q")

        if query:
            queryset = queryset.filter(
                Q(title__icontains=query) |
                Q(student__name__icontains=query)
                ).distinct()
            
            course_queryset = queryset.filter(title__icontains=query)
            student_queryset = Student.objects.filter(name__icontains=query)

        context = {
            'object_list': queryset,
            'student_object_list': student_queryset,
            'course_object_list': course_queryset
        }
        return render(request, self.template_name, context)


class CourseDeleteView(CourseObjectMixin, View):
    template_name = "courses/course_delete.html" 

    def get(self, request, id=None, *args, **kwargs):
        # GET Method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        # POST Method
        context = {}
        obj  = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('/courses/')
        return render(request, self.template_name, context)


class CourseUpdateView(CourseObjectMixin, View):
    template_name = "courses/course_update.html" 

    # def get_object(self):
    #     id = self.kwargs.get('id')
    #     obj = None
    #     if id is not None:
    #         obj = get_object_or_404(Course, id=id)
    #     return obj

    def get(self, request, id=None, *args, **kwargs):
        # GET Method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CourseModelForm(instance=obj)
            context['object'] = obj
            context['form'] = form

        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        # POST Method
        context = {}
        obj  = self.get_object()
        if obj is not None:
            form = CourseModelForm(request.POST, instance=obj)
            if form.is_valid():
                form.save()
                context['object'] = obj
                context['form'] = form

        return render(request, self.template_name, context)


class CourseCreateView(View):
    template_name = "courses/course_create.html" 

    def get(self, request, *args, **kwargs):
        # GET Method
        form = CourseModelForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST Method
        form = CourseModelForm(request.POST)
        if form.is_valid():
            form.save()
            form = CourseModelForm()
        context = {'form': form}
        return render(request, self.template_name, context)


class CourseListView(View):
    template_name = "courses/course_list.html"
    queryset = Course.objects.all()

    def get_queryset(self):
        # return super().get_queryset()
        return self.queryset
    

    def get(self, request, *args, **kwargs):
        context = {
            'object_list': self.get_queryset()
        }
        return render(request, self.template_name, context)


# class MyListView(CourseListView):
#     queryset = Course.objects.filter(id=1)


class CourseView(View):
    template_name = "courses/course_detail.html" # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET Method
        context = {}
        if id is not None:
            obj = get_object_or_404(Course, id=id)
            context['object'] = obj
        return render(request, self.template_name, context)

    # def post(request, *args, **kwargs):
    #     return render(request, 'about.html', {})


#BASE VIEW class = View

# class CourseView(View):
#     template_name = "about.html"

#     def get(self, request, *args, **kwargs):
#         # GET Method
#         return render(request, self.template_name, {})

#     # def post(request, *args, **kwargs):
#     #     return render(request, 'about.html', {})



# paginator = Paginator(contact_list, 25) # Show 25 contacts per page.
# page_number = request.GET.get('page')
# page_obj = paginator.get_page(page_number)
# return render(request, 'list.html', {'page_obj': page_obj})

# HTTP methods
def my_fbv(request, *args, **kwargs):
    return render(request, 'about.html', {})
