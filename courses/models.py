import datetime
from django.db import models
from django.urls import reverse
from students.models import Student

from mongoengine import [
                Document,
                StringField,
                IntField,
                ListField,
                ReferenceField,
                DateTimeField
                ]


class Course(models.Model):
    title = models.CharField(max_length=120)
    start_time = models.TimeField(default= time(10, 0, 0))

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("courses:course-detail", kwargs={"id":self.id})



# class Course(Document):
#     title = StringField(max_length=120, required=True)
#     start_time = DateTimeField(default= datetime.datetime.now(), required=True)
#     students_enrolled = ListField(ReferenceField(Student))

#     def __str__(self):
#         return self.title

#     def get_absolute_url(self):
#         return reverse("courses:course-detail", kwargs={"id":self.id})